// Gulpfile modules
const 	gulp = require('gulp'),
		sass = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		sourcemaps = require('gulp-sourcemaps'),
		cssNano = require('gulp-cssnano'),
		rsync = require('gulp-rsync'),
		plumber = require('gulp-plumber'),
		prompt = require('gulp-prompt'),
		del = require('del'),
		runSequence = require('run-sequence'),
		rev = require('gulp-rev'),
		revReplace = require('gulp-rev-replace'),
		browserify = require('browserify'),
		babelify = require('babelify'),
		buffer = require('vinyl-buffer'),
		source = require('vinyl-source-stream'),
		uglify = require('gulp-uglify'),
		svgSprite = require('gulp-svg-sprite'),
		htmlmin = require('gulp-htmlmin'),
		imagemin = require('gulp-imagemin'),
		injectSVG = require('gulp-inject-svg'),
		inject = require('gulp-inject'),
		gulpif = require('gulp-if'),
		fs = require('fs');

const 	browserSync = require('browser-sync').create();
const 	reload = browserSync.reload;

// Path variables
const 	path_src = 'src/',
		path_dist = 'dist/',
		path_img = 'images/',
		path_scripts = 'scripts/',
		path_styles = 'styles/',
		path_fonts = 'fonts/',
		path_icons = 'icons/',
		path_includes = 'includes/';

// SVG Config
var config_svg = {
	mode: {
		symbol: { // symbol mode to build the SVG
			render: {
				css: false, // CSS output option for icon sizing
				scss: false // SCSS output option for icon sizing
			},
			dest: '', // destination folder
			prefix: '.svg--%s', // BEM-style prefix if styles rendered
			sprite: 'icons.svg', //generated sprite name
			example: false // Build a sample page, please!
		}
	}
};

// Image Config
var plugins_imagemin = [
	imagemin.gifsicle({interlaced: true}),
	imagemin.jpegtran({progressive: true}),
	imagemin.optipng({optimizationLevel: 5}),
	imagemin.svgo({
		plugins: [
			{removeViewBox: true},
			{cleanupIDs: false},
			{convertTransform: false}
		]
	})
];

var config_imagemin = {
	verbose: true
};

var dist = false;

gulp.task('html', () => {
	return gulp.src(path_src + '**/*.html')
	.pipe(injectSVG({base: path_src}))
	.pipe(gulpif(dist,
		inject(gulp.src(path_src + 'includes/tracking.html'), {
			starttag: '<!-- inject:tracking -->',

			transform: function (filePath, file) {
				// return file contents as string
				return file.contents.toString('utf8')
			}
		})
	))
	.pipe(htmlmin({
		collapseWhitespace: true,
		minifyCSS: true,
		minifyJS: true
	}))
	.pipe(gulp.dest(path_dist));
});

gulp.task('images', ['icons'], () => {
	return gulp.src(path_src + '**/*.{jpg,png,gif,svg}')
	.pipe(imagemin(plugins_imagemin, config_imagemin))
	.pipe(gulp.dest(path_dist));
});

gulp.task('icons', () => {
	return gulp.src(path_src + path_img + path_icons + '**/*.svg')
	.pipe(svgSprite(config_svg))
	.pipe(gulp.dest(path_dist + path_img));
});

gulp.task('styles', () => {
	return gulp.src(path_src + path_styles + '**/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass()).on('error', sass.logError)
	.pipe(autoprefixer())
	.pipe(gulpif(dist, cssNano()))
	.pipe(gulpif(!dist, sourcemaps.write()))
	.pipe(gulp.dest(path_dist + path_styles))
	.pipe(reload({stream: true}));
});


gulp.task('scripts', () => {
	const b = browserify({
			entries: 'src/scripts/main.js',
			transform: babelify,
			debug: true
	});

	return b.bundle()
		.pipe(source('bundle.js'))
		.pipe(plumber())
		.pipe(buffer())
		.pipe(uglify({
			mangle: false,
			compress: false,
			output: {
				beautify: false
			}
		}))
		.pipe(gulp.dest(path_dist + path_scripts));
});

gulp.task('fonts', () => {
	return gulp.src(path_src + path_fonts + '**/*.{woff,woff2}')
	.pipe(gulp.dest(path_dist + path_fonts));
});

gulp.task('misc', () => {
	return gulp.src([path_src + '*/*', '!' + path_src + '*.html', '!' + path_src + path_fonts + '**/*', '!' + path_src + path_img + '**/*', '!' + path_src + path_scripts + '**/*', '!' + path_src + path_styles + '**/*'], {
		dot: true
	})
	.pipe(gulp.dest(path_dist));
});

// Build tasks
gulp.task('build', ['clean-prior'], () => {
	dist = true;
	runSequence('build:dist');
});

gulp.task('build:test', ['clean-prior'], (callback) => {
	dist = false;
	runSequence('build-sequence', callback);
});

gulp.task('build:preview', ['clean-prior'], (callback) => {
	dist = false;
	runSequence('build:dist', callback);
});

gulp.task('build-sequence', ['html', 'images', 'styles', 'scripts', 'fonts', 'misc'], () => {
});

gulp.task('build:dist', ['build-sequence:dist'], () => {
});

gulp.task('build-sequence:dist', ['html', 'images', 'styles', 'scripts', 'fonts', 'misc'], (callback) => {
	runSequence('replace', 'clean-after', callback);
});

// Misc tasks
gulp.task('default', () => {
	runSequence('build');
});

gulp.task('clean-prior', del.bind(null, [path_dist]));

gulp.task('revisions', () => {
	return gulp.src(path_dist + path_styles + '**/*.css')
	.pipe(rev())
	.pipe(gulp.dest(path_dist + path_styles))
	.pipe(rev.manifest())
	.pipe(gulp.dest(path_dist + path_styles));
});

gulp.task('replace', ['revisions'], () => {
	var manifest = gulp.src(path_dist + path_styles + 'rev-manifest.json')
	return gulp.src(path_dist + '**/*.html')
	.pipe(revReplace({manifest: manifest}))
	.pipe(gulp.dest(path_dist));
});

gulp.task('clean-after', del.bind(null, [
	path_dist + path_styles + 'rev-manifest.json',
	path_dist + path_styles + '!(*-*).?(css|css.gz)',
	path_dist + path_includes
]));

// Serve tasks
gulp.task('serve', ['build:test'], () => {
	browserSync.init({
		notify: false,
		open: false,
		port: 9402,
		server: {
			baseDir: path_dist
		}
	});

	gulp.watch([
		path_src + '**/*.html',
		path_src + path_img
	]).on('change', reload);

	gulp.watch(path_src + '**/*.html', ['html']);
	gulp.watch(path_src + path_img + '**/*', ['images']);
	gulp.watch(path_src + path_scripts + '**/*.js', ['scripts']);
	gulp.watch(path_src + path_styles + '**/*.{css,scss}', ['styles']);
});

gulp.task('serve:dist', ['build:dist'], () => {
	browserSync.init({
		notify: false,
		open: false,
		port: 9403,
		server: {
			baseDir: path_dist
		}
	});
});

// Deploy tasks
rsyncPaths = [path_dist];

gulp.task('deploy', ['build:preview'], () => {
	var credentials = JSON.parse(fs.readFileSync('rsync-credentials.json', 'utf-8'));
	return gulp.src(rsyncPaths)
	.pipe(prompt.confirm({
		message: 'Push to ' + credentials.destination + '?',
		default: false
	}))
	.pipe(rsync({
		root: path_dist,
		hostname: credentials.hostname,
		username: credentials.username,
		destination: credentials.destination,
		progress: true,
		incremental: true,
		relative: true,
		emptyDirectories: true,
		recursive: true,
		clean: true,
		exclude: ['robots.txt']
	}));
});